---
title: New maintainer
author: Allan Nordhøy
layout: single
permalink: /new-maintainer/
excerpt: Maintain ease and keep calming
categories:
  - News
  - Announcement
tags:
  - announcement
---

Put on your best tanooki suit, there is a new maintainer in town. \
Like a visit to a vending machine that has maintainers, \
we got exactly what we wanted. \
No longer one-horse towned in the animal farm sense, \
Remmina-ville is safe again.
Down from a cloud-platform jumped myheroyuki \
to press both A and B on us all, \
via first bopping some bugs, linting some lints, \
and featuring.

You are probably familiar with Takanaka (the muscian), \
but this is _Tanaka_. Notice how it is almost the same. \
It is a different person. \
Yes, a cover band has taken over the player 1 controller for Remmina!

With that, the gnomes are defeated for now. \
Thank you to everyone sending nice messages and coins to collect.

---
title: Remmina The Digger Edition
date: 2017-12-28T00:53:24+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-the-digger-edition/
excerpt: Remmina release announcement
categories:
  - News
  - Release
tags:
  - release
  - remmina
---
<img class="aligncenter wp-image-2491 size-full" src="/assets/images/Remmina_The_Digger_s.png" alt="" width="600" height="377" srcset="/assets/images/Remmina_The_Digger_s.png 600w, /assets/images/Remmina_The_Digger_s-300x189.png 300w" sizes="(max-width: 600px) 100vw, 600px" />

A new Remmina version was released, solving most of the issues with the SSH protocol plugin, a new Exec protocol plugin and several bugfixes.

The above image and the release attempts to highlight both, Digging through the source code pays off.

For those interested in all the arcane details, dig through the <a href="https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.archive.md" target="_blank" rel="noopener">CHANGELOG.md file</a>

Of note,

**Implemented enhancements:**

  * Feature request &#8211; Option to completely hide the floating toolbar when in fullscreen. [#1379](https://gitlab.com/Remmina/Remmina/-/issues/1379)
  * Option to run pre command before anything else [#1363](https://gitlab.com/Remmina/Remmina/-/issues/1363)
  * Enhancement &#8211; Variables for pre- and post-commands [#849](https://gitlab.com/Remmina/Remmina/-/issues/849)
  * Exec protocol plugin [#1406](https://gitlab.com/Remmina/Remmina/-/merge_requests/1406) ([antenore](https://github.com/antenore))
  * SPICE Native WebDAV shared folder support [#1401](https://gitlab.com/Remmina/Remmina/-/merge_requests/1401) ([larchunix](https://github.com/larchunix))
  * Encryption algorithms options for SSH [#1397](https://gitlab.com/Remmina/Remmina/-/merge_requests/1397) ([antenore](https://github.com/antenore))
  * Prior commands improvements [#1378](https://gitlab.com/Remmina/Remmina/-/merge_requests/1378) ([antenore](https://github.com/antenore))
  * flatpak: Manifest for flatpak-builder [#1368](https://gitlab.com/Remmina/Remmina/-/merge_requests/1368) ([larchunix](https://github.com/larchunix))
  * Telepathy: properly setup dbus activation [#1365](https://gitlab.com/Remmina/Remmina/-/merge_requests/1365) ([larchunix](https://github.com/larchunix))
  * Added Avahi host discovery for SSH and SFTP plugins [#1355](https://gitlab.com/Remmina/Remmina/-/merge_requests/1355) ([larchunix](https://github.com/larchunix))

**Fixed bugs:**

  * Un-checked &#8220;fullscreen on the same monitor as the connection window&#8221; won&#8217;t save [#1344](https://gitlab.com/Remmina/Remmina/-/issues/1344)
  * Remmina resets screen resolution settings in RDP shortcut [#1323](https://gitlab.com/Remmina/Remmina/-/issues/1323)
  * SSH tunneling is broken with SSH agent with public key [#1228](https://gitlab.com/Remmina/Remmina/-/issues/1228)
  * Failed to load plugin: remmina-plugin-telepathy.so &#8211; undefined symbol: remmina [#714](https://gitlab.com/Remmina/Remmina/-/issues/714)
  * Align SFTP and SSH plugins authentication and tunnel functionalities. [#1393](https://gitlab.com/Remmina/Remmina/-/merge_requests/1393) ([antenore](https://github.com/antenore))
  * Fix Telepathy plugin compilation [#1356](https://gitlab.com/Remmina/Remmina/-/merge_requests/1356) ([larchunix](https://github.com/larchunix))
  * \_\_func\_\_ keyword must not be quoted [#1350](https://gitlab.com/Remmina/Remmina/-/merge_requests/1350) ([larchunix](https://github.com/larchunix))

**Closed issues:**

  * SSH not working [#1418](https://gitlab.com/Remmina/Remmina/-/issues/1418)
  * Unable to reject new or changed RDP certificate [#1413](https://gitlab.com/Remmina/Remmina/-/issues/1413)
  * The password in the connections is not saved after upgrading Remmina [#1402](https://gitlab.com/Remmina/Remmina/-/issues/1402)
  * Remmina fails to open sftp window, connected to ssh2 server with public key auth [#1392](https://gitlab.com/Remmina/Remmina/-/issues/1392)
  * WARNING: The &#8220;resolution&#8221; setting in .pref files is deprecated [#1358](https://gitlab.com/Remmina/Remmina/-/issues/1358)
  * Remmina shows pop-up notification only for first screenshot [#1347](https://gitlab.com/Remmina/Remmina/-/issues/1347)
  * SFTP identity File [#1301](https://gitlab.com/Remmina/Remmina/-/issues/1301)
  * Password not saved [#1047](https://gitlab.com/Remmina/Remmina/-/issues/1047)

**Merged pull requests:**

  * Fixes 2017 yule [#1416](https://gitlab.com/Remmina/Remmina/-/merge_requests/1416) ([antenore](https://github.com/antenore))
  * New Venezuelan Spanish translation [#1415](https://gitlab.com/Remmina/Remmina/-/merge_requests/1415) ([jgjimenez](https://github.com/jgjimenez))
  * Improved Danish translation [#1411](https://gitlab.com/Remmina/Remmina/-/merge_requests/1411) ([scootergrisen](https://github.com/scootergrisen))
  * Improved Spanish translation [#1410](https://gitlab.com/Remmina/Remmina/-/merge_requests/1410) ([fitojb](https://github.com/fitojb))
  * Improved French translation [#1409](https://gitlab.com/Remmina/Remmina/-/merge_requests/1409) ([DevDef](https://github.com/DevDef))
  * Improved Simplified Chinese translations. [#1367](https://gitlab.com/Remmina/Remmina/-/merge_requests/1367) ([sotux](https://github.com/sotux))
  * ssh\_userauth\_publickey_auto: Should accept empty passphrase [#1361](https://gitlab.com/Remmina/Remmina/-/merge_requests/1361) ([rayrapetyan](https://github.com/rayrapetyan))
  * Remove a couple of legacy &#8220;resolution&#8221; fields, fixes #1358 [#1360](https://gitlab.com/Remmina/Remmina/-/merge_requests/1360) ([giox069](https://github.com/giox069))
  * Un-check of &#8216;Fullscreen on the same monitor&#8217; [#1349](https://gitlab.com/Remmina/Remmina/-/merge_requests/1349) ([antenore](https://github.com/antenore))
  * Create CODE\_OF\_CONDUCT [#1341](https://gitlab.com/Remmina/Remmina/-/merge_requests/1341) ([antenore](https://github.com/antenore))

Enjoy it and happy new remote Year!!!!

---
title: Remmina Kiosk Edition
date: 2018-07-31T11:35:40+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-kiosk-edition/
excerpt: Explaining the new release, move to GitLab, and kiosk mode.
categories:
  - announcement
  - News
tags:
  - kiosk
  - release
---

{% include figure image_path="/assets/images/vacations.jpg" %}

Despite it is vacation time and quite too hot to stay at home coding,
this deterred nobody in releasing a new Remmina version.

It is not that big, as we were busy moving from the proprietary [GitHub](https://github.com)
to the libre softweare [GitLab](https://gitlab.com/Remmina/Remmina), following the formers purchase by Microsoft.
Nevertheless, we where able to introduce a kiosk mode, adding custom SSH terminal colors per profile, and many bugfixes.

The details:

- Custom color schemes per profile for the SSH plugin (@denk_mal).
- Flatpak updates and fixes (@larchunix)
- Kiosk mode with integration in the login manager (@antenore).
- New icons (@antenore and @larchunix).
- SFTP tool password fixes (@Feishi).
- Several fixes around RDP and compilations issues (@giox069 and @larchunix).

The continuos integration is up and running, which partly generates the Remmina
packages you use.

As usual, to install and/or upgrade, follow our [wiki](https://gitlab.com/Remmina/Remmina/wikis/home)
and don't hesitate to report bugs or make feature requests.

---
title: Remmina 0.7 released!
date: 2009-12-12T08:42:00+00:00
author: Antenore Gatta
layout: single
excerpt: Historical Remmina blog article
permalink: /remmina-0-7-released/
categories:
  - News
  - announcement
tags:
  - release
---
The first stable release of Remmina is now available. Feel free to download and post comments!

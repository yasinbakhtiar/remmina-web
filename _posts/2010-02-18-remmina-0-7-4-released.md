---
title: Remmina 0.7.4 released!
date: 2010-02-18T08:45:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-7-4-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
Various memory issues corrected in this release.

---
title: Elective eclecticism
author: Allan Nordhøy
layout: single
permalink: /elective-eclecticism/
excerpt: 1.4.9 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

New faces in Remminaland, [kapsh](https://gitlab.com/kapsh), [bwack](https://gitlab.com/bwack) and [Fantu](https://gitlab.com/Fantu) all join the campaign of world foreordination.

The Remmina orbit has accumulated some fixes over time, and skips ahead to the present state for its release in due time. Gentoo and Mageia Cauldron have already [erected the 1.4.9 flag](https://repology.org/badge/vertical-allrepos/remmina.svg?columns=4&minversion=1.3?allow_ignored=1).

The https://liberapay.com/Remmina agglutination sits at exactly 100 contributors, connected to a purpose in life. It is not yet known whether this makes you a better person in the ideal of the good of man, or either of those, but those 100 must have found some stable happiness in contributing, considering the relative mass of people that aren't those, and the problems other people have.
It just so happens most of the world's problems lie with those that aren't donating, so one bird lifts
two stones here. It may though in the grander scale of things be a bit premature to suggest it will solve _all_ your problems, but there is feasibly some happiness to be found, in this life and all possible other ones. People donating continue to do so, so they in the very least aren't dying. So you may find, _donating grants eternal life_.

Erring cautiously on the side of cosmic chance as the reasoned remminers people are,
it is reasonable here to interject that the premise of it holding up in terms of sudden illness striking
is likely to change, given more takers. Yet, that should in either case mean getting in now grants you the best possible scenario of playing the odds.
We further must only assume donators are putting themselves at the peril of
life itself to donate. Or, more likely, they just have disposable income, and
who wouldn't want to be in that group? ;)

The people and maybe one contested vote wins https://www.slant.co/topics/2517/~best-remote-desktop-solutions-for-linux by crowning Remmina the one true remote desktop of the people (that voted). The results of [UbuntuPIT](https://www.ubuntupit.com/fast-and-secure-remote-desktop-clients-for-linux/), [Make Tech Easier](https://www.maketecheasier.com/best-linux-remote-desktop-apps/) and [DebugPoint](https://www.debugpoint.com/2020/03/best-remote-desktop-software/) can all be said to be valid, if not also somewhat discriminately picked ;)

One reason to dislike Pulseaudio a litte less (bear with me) is that it now works for the Remmina Snap.
You will have an easier time, with improved scrolling, colours, language, log filtering, screenshot naming, text-search in the SSH plugin, typing in your IP, documentation, setting up a kiosk, and saving preferences

If you haven't had any of these problems, you can now continue not having these problems, as you were.

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.9) for 1.4.9 has a bit of everything, recursively rippling into the future from the past. From a neutral standpoint, the next changelog is always a good place to look for things in need of fixing, but some fixes target the timely sidereal 1.5.0 release upfront by possibly unlocking the multi-monitor.

119 new [translations](https://hosted.weblate.org/projects/remmina/remmina/) from the country of Žižek and so on and so forth, and also 119 points towards Diogenes syndrome for Greek :)

Update: Some people would walk and put a foot to the side for each step, provided it granted better stability. Here at Remmina, a confident and speedy stride is aimed at and by progress, even
if it detours releases towards finding fixes said to be hot as we get close to
finding out whether bugs are bugs in the aggregate, or aggregated in the finding out.

If you get a black screen when connecting to a remote Windows server, [set "Redirect local microphone" to nothing instead of 0](https://gitlab.com/Remmina/Remmina/-/wikis/Problems-and-tweaks/Black-screen).
---
title: The winter warmth
author: Allan Nordhøy
layout: single
permalink: /winterwarmth/
excerpt: 1.4.28 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---
The times are unchanging.
This time in familiar times, new becomes old, and old is new again.
As familiar goes, it can get both repetitive, or predictable,
but in contrast to the new and unmanaged, it all feels welcoming.

Lots of changes, with a healthy set of helpers.
Maintain what spurs growth, and rest.
The known, true and trusted, in improved form.

An incipient [stocked fireplace](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.28) of festivity and happiness.

Take some time out of your day to appreciate and improve your surroundings.

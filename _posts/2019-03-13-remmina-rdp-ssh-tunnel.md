---
title: Remmina RDP SSH Tunnel
author: Antenore Gatta
layout: single
permalink: /remmina-rdp-ssh-tunnel/
excerpt: How to set up an RDP connection through an SSH tunnel, using pre and post commands
categories:
  - article
  - howto
tags:
  - howto
---

[Kgibran](https://kgibran.wordpress.com/) published an interesting article on how to set up an SSH tunnel, using the pre/post Remmina commands, to connect to an RDP server.

The pre and post commands is a feature used to execute a command or a script, just before or after a Remmina connection is initiated/closed. So, for instance, if you need to do actions like:

- Start or stop a VM
- Start or stop an SSH tunnel
- Start or stop a VPN

The pre and post commands are really handy.

{% include figure image_path="https://kgibran.files.wordpress.com/2019/03/remmina-rdp-edit-1.png?w=460" %}

[Enjoy the article](https://kgibran.wordpress.com/2019/03/13/remmina-rdp-ssh-tunnel-with-pre-and-post-scripts/), say thanks and hello to Kgibran.

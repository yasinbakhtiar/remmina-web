---
title: 'Kenyan hospitals, with Massimo and Remmina.'
date: 2017-02-12T17:24:36+00:00
author: Dario Cavedon
layout: single
excerpt: Article showing how we helped a Kenya hospital set up thin client architecture
permalink: /kenyan-hospitals-with-massimo-and-remmina/
categories:
  - Testimonials
tags:
  - hospital
  - Kenya
  - testimonials
---
<img class="aligncenter wp-image-2226 size-large" src="/assets/images/north-kinangop01-1024x533.jpg" alt="North Kinangop Catholic Hospital" width="640" height="333" srcset="/assets/images/north-kinangop01-1024x533.jpg 1024w, /assets/images/north-kinangop01-300x156.jpg 300w, /assets/images/north-kinangop01-768x399.jpg 768w, /assets/images/north-kinangop01.jpg 1240w" sizes="(max-width: 640px) 100vw, 640px" />

Starting work on Remmina back in 2014, indeed the aim was solving issues experienced ourselves.

In 2017, the state of Remmina is that of, still, little annoying problems, but **better than ever**, and **used by thousands** all over the world.

One of those, our new friend **Massimo**, volunteers for the “<a href="https://www.northkinangop.com/index.php/en/" target="_blank">North Kinangop Catholic Hospital</a>” in Kenya. We met him as he just came home. He lives in Padua (Italy), and we had coffee together.

The hospital he volunteers at is located about 130 kilometers north of Nairobi, on the plateau of Nyandarua, at the foot of the Aberdare range. It’s not only a hospital, but an entire autonomous community with houses, nursing school, mechanical workshop and many other laboratories, more than 350 beds and 4 operating rooms.

In the hospital, a custom program is used to enter data from Raspberry Pi boards connected to their main server **via Remmina**.<img class="alignright wp-image-2229" src="/assets/images/north-kinangop02-1024x783.jpg" alt="Remmina at “North Kinangop Catholic Hospital”" width="499" height="382" srcset="/assets/images/north-kinangop02-1024x783.jpg 1024w, /assets/images/north-kinangop02-300x229.jpg 300w, /assets/images/north-kinangop02-768x587.jpg 768w, /assets/images/north-kinangop02.jpg 1046w" sizes="(max-width: 499px) 100vw, 499px" />
When Massimo contacted us, they were using an old version of Remmina 1.0, running on Raspbian. Remmina 1.0 has a lot of bugs, and it is out of the maintenance cycle, so Massimo sent us an e-mail asking for help.

With our remote support, in only a few hours **he easily upgraded their Raspberry Pi boards to Xubuntu 16.04 and Remmina 1.2**.  _“When you are over there, and you’re away from everything, you have to carry all you need from Italy. When I was in trouble with Remmina, I sent a message to the maintainers, and didn’t expect anything in particular, but within few hours it all was up and running!”_. Thanks Massimo for your kind words! **We are proud to help others!**

**_Are you using <span class="il">Remmina</span> in your organization or association? Send us some words about your experience, and we can publish it here._**

---
title: Remmina places 3rd in 2018 TechRadar review
date: 2018-05-02T12:46:56+00:00
author: Dario Cavedon
layout: single
permalink: /remmina-places-3rd-in-2019-techradar-review/
excerpt: Remmina article discussing the TechRadar review
categories:
  - News
tags:
  - RDC
  - RDP
  - reviews
  - TechRadar
---

The original post was [here](/remmina-places-2nd-in-2018-tech-radar-review/)

<img class="aligncenter wp-image-2553 size-full" src="/assets/images/techradar2018.jpg" alt="Tech Radar 2018 review" width="1266" height="766" srcset="/assets/images/techradar2018.jpg 1266w, /assets/images/techradar2018-300x182.jpg 300w, /assets/images/techradar2018-768x465.jpg 768w, /assets/images/techradar2018-1024x620.jpg 1024w" sizes="(max-width: 1266px) 100vw, 1266px" />

TechRadar [wrote their periodic review](https://web.archive.org/web/20180626155940/https://www.techradar.com/news/5-of-the-best-linux-remote-desktop-clients) of the **“Best Linux remote desktop clients: Top 5 RDC in 2018”**. The good news is that **Remmina is one of them**, placed **third** (of five), behind TigerVNC, and the similar TightVNC.

TechRadar wrote:

> _“Remmina scores decently in the performance department and gives you the flexibility to change connection quality settings on the fly. **The client supports the widest range of protocols and will connect to all kinds of remote desktop servers**. Remmina doesn’t have its own server software, but you can use it for all sorts of remote connections to all kinds of servers. Sadly, Remmina lacks some extra functionality that you get with other clients such as the ability to transfer files.”_

Well, they tested Remmina and others using **only VNC**, that’s not the primary Remmina business, but we are happy to be among the best.

What do you think about it? Do you think we have to improve Remmina’s VNC experience? Share your thoughts with us, and leave a comment below!

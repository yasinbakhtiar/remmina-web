---
title: Work from home
author: Allan Nordhøy
layout: single
permalink: /work-from-home/
excerpt: Stay safe, work hard.
categories:
  - News
  - Announcement
  - Corona
  - Hints
tags:
  - Safety
---
To prevent the spread of the Corona virus (COVID-19),
it is now common for companies to put in place policies
requiring people to work from home.

This is something Remmina does, which we
found it reasonable to recommend, following
internal consideration looking at the guidelines
of governments, municipalities, and the numbers of
infected people around the world,

Connect to your work-computer from home, and
do what needs doing, just like you otherwise would,
only now using the computer you have available,
by installing Remmina on it.

In addition to the usual, consider also that your
keyboard and your mouse are areas of physical contact
you should avoid sharing with others.

Outside of more serious threats like SARS and COVID-19,
having the ability of doing home-office days additionally
lowers the amount of sick-days, so having the
capability ready to go comes in handy in any event.

This way, there is no need to come in to infect others
unnecessarily, even when you are feeling a bit better,
or aren't totally under the weather.

We are not trying to sell you Remmina, and hope
our concern for your physical and digital safety
is (optionally) worthy of a donation only if you find
the product to be of use to you.

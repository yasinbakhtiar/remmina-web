---
title: Future consumed in the machine
author: Allan Nordhøy
layout: single
permalink: /inmachine/
excerpt: Tragic projections least affected.
categories:
  - News
  - announcement
tags:
  - news
---

All wanting to get out, but none daring to stop at the intersection of approached sustainability
in something made a certain way.
Removed from how its parts were sourced, how easily they can be put together or taken apart,
it all still requires users to prolong its death.
So much so that most things or decisions leading to them just _aren't_,
as a result of nobody willing to do it, reality getting in the way or passing by.
It is all a series of interconnected experience, and it is a bus.

These many tiered approaches in some respects represent
where any project is at in the scheme of things, as compared to other offerings.
Any past where the original timeline wasn't made and doesn't now
continue being sold would not pave the way for what is truly
underestimated.

When thinking of what one doesn't get elsewhere, it is also something to
be kept in mind for what "the product" is as something different to what is available.
The extreme of one common power metric often doesn't make for a good tool everywhere,
because as only counterproductive — it is something otherwise offered better elsewhere.
Compromises similarly do exist.
Hand in as much freedom as you like.

The best camera is the one you have in your hands.
Meanwhile most expensive cameras and heavy lenses are producing negative space in some drawer,
producing no greater imagery and imagination than realizing it shouldn't be there.
Before it could sit there dormant for a while without issue, but a user-problem isn't really
solved by having to empty the shelves for new wares every so evermore often.

What a great proposal for one more, or the generic plural mantra of "new product".
Starting with full shelves with this one, emptying them should bring some level of
nostalgia at best. Throwing away the smart toaster is a duty to fulfil life like
it did in the commercial nobody wanted to see for the tenth time.
Despite more venues of input, more things promising to bring about a new age
is something that will be typed about in public for years to come.
You will eat the smartest toast, and have smart thoughts.
"What a time to be alive", you will not think to yourself, as repeated by staff at the behest of
corporate policy at the onset of your indulgence.

The distance is closed for when there is time already killed in for example transporting oneself
somewhere at the helm, joystick, autopilot, or wheel of someone else.
Maybe you will even be a good product for a good hour or two?
Oh, but the battery lasts longer, yes.
Sometimes an "emergency" as measured by relative escape from society occurs.
Then it may be your only hope in fending off loosened elective mind-control. How often?
Less often than not aimlessly trying to recharge after every futile use one could be permitted to think.
For every hour typed away, there are minutes and sanity spared in not trying to
make it a continuous two. For all sanity lost, time spent ceases to control anything of value.
Your private thoughts can not be stripped from you if you become the cloud.

It is one thing to envision a conceptual idea of utility, but rather another to have albeit at a price-premium, to be a still approachable embodiment of product placement.
This is the new old. It is meant to get old. This is how it used to be.
You can get "the future" if you want to. The future is "a word inserted", and that word is "bleak" as far
as I can tell. Bleak.

The fourth wall sells you this idea for free: How much would someone have to care about their users to do this?
Surely the big players are not going in the totally opposite direction, together?
After all it says moral purchases from them create of you an ecosystem of greater repairability, less
surveillance, more freedom, and fewer things focused on passive consumption rather than creative
space and output? It must be true.

Granted, you are already here, but where along the way marches your tech-support for your current (ensnaring) circle?
Fridge says "No"? New computer is as soulless as its intentions? Faucet knows about your wireless network
and your online purchase history? "New thing bad, new thing bad" you scream, and the rotating soup stirrer listens.

They are all waiting for it. It is about that time — the incentive to start, continue or increase doing something.
Measured use of a personal computer is required for anything that can call itself a person,
which at that can point out its non-inherent but bundled flaws in maybe such a way as to appear truly human.

For any problem worth your time so much so that you aren't getting rid of it, the
stuff outside of the box your problem is or came in always works better.

Buy, (buy, buy) this computer, and let your cat play with the packaging if you want to know how to use it. \
https://www.crowdsupply.com/mnt/pocket-reform

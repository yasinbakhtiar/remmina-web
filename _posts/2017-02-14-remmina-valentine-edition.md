---
title: Remmina Valentine Edition
date: 2017-02-14T09:20:03+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-valentine-edition/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - release
  - remmina
---
![Sweetheart](/assets/images/valentine-1024x672.jpg){.aligncenter
.wp-image-2240 .size-large width="640" height="420"
sizes="(max-width: 640px) 100vw, 640px"
srcset="/assets/images/valentine.jpg 1024w, /assets/images/valentine-300x197.jpg 300w, /assets/images/valentine-768x504.jpg 768w"}
With love from The Remmina, the Remmina valentine edition:

Changelog
=========

[v1.2.0-rcgit.18](https://github.com/FreeRDP/Remmina/tree/v1.2.0-rcgit.18) (2017-02.14)
---------------------------------------------------------------------------------------

[Full
changelog](https://github.com/FreeRDP/Remmina/compare/v1.2.0-rcgit.17...v1.2.0-rcgit.18)

**Enhancements:**

-   No prompt info while another login is using the same account
    [\#1114](https://gitlab.com/Remmina/Remmina/-/merge_requests/1114)
    ([nanxiongchao](https://github.com/nanxiongchao))
-   Snap: Add CMake generated snapcraft.yaml and `make snap`
    [\#1102](https://gitlab.com/Remmina/Remmina/-/merge_requests/1102)
    ([3v1n0](https://github.com/3v1n0))

**Fixed bugs:**

-   fullscreen window placement with multiple monitors
    [\#124](https://gitlab.com/Remmina/Remmina/-/issues/124)

**Closed issues:**

-   Auto-reconnect function
    [\#1099](https://gitlab.com/Remmina/Remmina/-/issues/1099)
-   RDP plugin not found after upgrade
    [\#1094](https://gitlab.com/Remmina/Remmina/-/issues/1094)
-   Remmina-1.2.0-rcgit.17 build error
    [\#1090](https://gitlab.com/Remmina/Remmina/-/issues/1090)
-   Remote Windows program crashes when accessed by Remmina RDP
    [\#1083](https://gitlab.com/Remmina/Remmina/-/issues/1083)
-   Crashing since 1.2.0-rcgit-17 upgraded
    [\#1077](https://gitlab.com/Remmina/Remmina/-/issues/1077)
-   Remmina windows unmovable/unresizable
    [\#1073](https://gitlab.com/Remmina/Remmina/-/issues/1073)
-   Improved Russian translation
    [\#1071](https://gitlab.com/Remmina/Remmina/-/issues/1071)
-   Improved English strings
    [\#1040](https://gitlab.com/Remmina/Remmina/-/issues/1040)
-   Compile failed on Archlinux
    [\#1012](https://gitlab.com/Remmina/Remmina/-/issues/1012)
-   RDP connections or whole Remmina are crashing regularly
    [\#778](https://gitlab.com/Remmina/Remmina/-/issues/778)
-   xfce4-applet gone?
    [\#609](https://gitlab.com/Remmina/Remmina/-/issues/609)
-   Bad port stored in known\_hosts2
    [\#604](https://gitlab.com/Remmina/Remmina/-/issues/604)
-   No connection to printer from Remmnia
    [\#578](https://gitlab.com/Remmina/Remmina/-/issues/578)
-   Uzbek language support
    [\#560](https://gitlab.com/Remmina/Remmina/-/issues/560)
-   False message that VNC plugin is not installed
    [\#559](https://gitlab.com/Remmina/Remmina/-/issues/559)

**Merged pull requests:**

-   Snap: use snapcraft 2.26 features
    [\#1115](https://gitlab.com/Remmina/Remmina/-/merge_requests/1115)
    ([3v1n0](https://github.com/3v1n0))
-   Keyboard capture fixes,
    [\#1087](https://gitlab.com/Remmina/Remmina/-/issues/1087)
    [\#1096](https://gitlab.com/Remmina/Remmina/-/issues/1096)
    [\#1111](https://gitlab.com/Remmina/Remmina/-/issues/1111 "Remmina Next is Corrupting the Unity Desktop")
    [\#1113](https://gitlab.com/Remmina/Remmina/-/merge_requests/1113)
    ([giox069](https://github.com/giox069))
-   Keyboard capture changes,
    [\#1087](https://gitlab.com/Remmina/Remmina/-/issues/1087)
    [\#1096](https://gitlab.com/Remmina/Remmina/-/issues/1096)
    [\#1111](https://gitlab.com/Remmina/Remmina/-/issues/1111 "Remmina Next is Corrupting the Unity Desktop")
    [\#1112](https://gitlab.com/Remmina/Remmina/-/merge_requests/1112)
    ([giox069](https://github.com/giox069))
-   Travis: Add parallel builds to build from debs and generate Snap
    packages
    [\#1104](https://gitlab.com/Remmina/Remmina/-/merge_requests/1104)
    ([3v1n0](https://github.com/3v1n0))
-   GUI enhancements
    [\#1103](https://gitlab.com/Remmina/Remmina/-/merge_requests/1103)
    ([antenore](https://github.com/antenore))
-   Add descriptions for some FreeRDP exit status code
    [\#1101](https://gitlab.com/Remmina/Remmina/-/merge_requests/1101)
    ([giox069](https://github.com/giox069))
-   FindFREERDP.cmake: update library names to match upstream
    [\#1095](https://gitlab.com/Remmina/Remmina/-/merge_requests/1095)
    ([3v1n0](https://github.com/3v1n0))
-   Improved French translation
    [\#1089](https://gitlab.com/Remmina/Remmina/-/merge_requests/1089)
    ([DevDef](https://github.com/DevDef))
-   English typos fixed, as per
    [\#1040](https://gitlab.com/Remmina/Remmina/-/issues/1040)
    [\#1088](https://gitlab.com/Remmina/Remmina/-/merge_requests/1088)
    ([antenore](https://github.com/antenore))
-   Fullscreen in the same monitor as connection window resides
    [\#1084](https://gitlab.com/Remmina/Remmina/-/merge_requests/1084)
    ([antenore](https://github.com/antenore))

\#\# Downloads

-   [**Source code**
    (.zip)](https://github.com/FreeRDP/Remmina/archive/v1.2.0-rcgit.18.zip)
-   [**Source code**
    (.tar.gz)](https://github.com/FreeRDP/Remmina/archive/v1.2.0-rcgit.18.tar.gz)

---
title: Remmina 0.7.5 released!
date: 2010-05-02T08:45:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-7-5-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
This will be the last 0.7 stable release, unless any critical issue is discovered. The next major release (0.8) is close.

---
title: The depths of
author: Allan Nordhøy
layout: single
permalink: /depths/
excerpt: mortality itself
categories:
  - News
tags:
  - Poemelemic
  - Opinion
---

The interface to life has given available input, collateral direction.

Framed in terms of perceptual time, in a sequencing of events.

The shift in prescience, replaced by itself alas.

The coming anticipated giving, as much as accepting demise.

Transmitted embodiment of understanding.

Thought collectively cheapening ones perception of enriching wins.

In the preponderance of existence, short of inevitability.

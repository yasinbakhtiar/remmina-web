---
title: A fire in the time of
author: Allan Nordhøy
layout: single
permalink: /release-anew/
excerpt: 1.4.0 released.
categories:
  - News
  - Announcement
  - Release
  - Fundraiser
tags:
  - release
---
In the cycle of things, 1.4.0 is the erection of a warded long term release to rally around,
bringing with it RDP clipboard and SSH refactoring, in a sound goodbye to PPA releases.
This in the precense of the solution following it, Flatpak and SNAP, along with the tried and true,
traditional packaging.

As ever, PPA is only a concern of, Ubuntu users, the Swedish speaking among which will find a completed
translation has made it to the fresh SNAP package, curiously a mere 14 seconds too late for the PPA.

The full changelog [for 1.4.0](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.0) has been acosted from the
underworld, upon which this monument is raised, from the hard work of all involved.
Born anew, in the tradition and following development of what came before it,
[phryctoriaic feedback](https://remmina.org/contributing-to-remmina/) and [philanthropic funding](https://remmina.org/donations/) is a great help.

Void it is, darkness cast, shadow coming to, time alight.
Alight time, to coming shadow, cast darkness, is it void?

You are hoped to find a congruence of your liking in 2020.
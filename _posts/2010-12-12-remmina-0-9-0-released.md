---
title: Remmina 0.9.0 released!
date: 2010-12-12T08:49:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-9-0-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
This release is mainly focused on enhancing the main program. A tray icon was added, with even more features than the original panel applet package. As such, the panel applet packages are removed from 0.9. Please read the [Remmina 0.9.0 release notes](https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.archive.md) for more details.

---
title: Finespun refinery
author: Allan Nordhøy
layout: single
permalink: /finespun-refinery/
excerpt: 1.4.8 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

There is a new Remmina out there,
this time in the form of Snap, and Flatpak packaging, dropping the [PPA](https://remmina.org/oh-flatpak/) sidekick lagging behind.
The Flatpak is as always in colour, but now back in [stereophonic sound](https://gitlab.com/Remmina/Remmina/-/issues/2115#note_395855690) where available.

Turning the dials from 1.4.7 to 1.4.8 picks up many interesting and remarkable signals.
Tidak ada angin tidak ada hujan, tiba-tiba Badan, Ibu Penjual Bakso Ini. Kenapa?
The new [Indonesian translation](https://hosted.weblate.org/projects/remmina/remmina/id/) unlocks the biggest language in a nation of 280 million people.
There is room for growth. Happily Remmina is sitting at 398718 installs in the Ubuntu [popularity contest](https://popcon.ubuntu.com/). Canonical has removed the Popcon package from default Ubuntu installs, so a drop in the numbers is actually a good thing.

In other numbers on the rise, the 50€ donation goal was met and surpassed [on Liberapay](https://liberapay.com/Remmina/donate). That one is not just a number, as a lot of consideration goes in, and a lot of appreciation comes out of it. No less so in turn, as these transactions are the financial underpinnings of [multi-monitor support](https://gitlab.com/Remmina/Remmina/-/merge_requests/2043/commits) ambitions. You know it to be the future. Soon coming to multiple monitors near you.

There are many new code contributors, all of which are aesthetes mirrored in their creation, making the operation a refinery of delicate changes at the hands of the finest of remminerds.
The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.8) for 1.4.8 has good swirls, character and down to earthy transparency.

Nongkrong. Rejoice responsibly :)
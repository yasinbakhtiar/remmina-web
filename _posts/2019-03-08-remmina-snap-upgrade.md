---
title: Remmina Snap Upgrade
author: Antenore Gatta
layout: single
permalink: /remmina-snap-upgrade/
excerpt: Remmina release announcement
categories:
  - howto
  - News
tags:
  - Snap
  - howto
  - upgrade
---

The 7th og March 2019, thanks to the help of [Ken VanDine](http://ken.vandine.org/), there is now a Remmina Snap package.

This version fixes many of the issues, and it is built with the latest GNOME, FreeRDP and libssh library versions.

Some users report not being able to upgrade the Snap, probably resulting from a package name conflict under investigation.

If you encounter similar issues, try to do the following:

```shell
sudo snap remove remmina
sudo snap refresh
sudo snap install remmina
snap run remmina
```

Before refreshing the Snap, be sure to upgrade your distribution.



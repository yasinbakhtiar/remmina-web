---
title: Remmina is looking for new maintainers
author: Antenore Gatta
layout: single
permalink: /looking-for-maintainers/
excerpt: Time has come to say goodbye
categories:
  - News
  - Announcement
tags:
  - announcement
---

Hello all,

As you might have noticed, the project is stagnating.
There's a main reason for that, and the reason is me.

I no longer have time to track GTK advancements, for which we need to rewrite most of the Remmina code base.
Additionally, I don't use that much Remmina, as my job role is evolving.
Lastly, I lose interest in using GTK.

Giovanni, like me, is extremely busy with his job, and while we could submit some fixes from time to time, we cannot assure anymore the level of support we provided previously.

Therefore, we are looking for new maintainers, and in the meantime, we will remove some functionalities that cost us money (and/or time).

In the coming weeks and months, you will get some new Remmina releases where we will remove these functions:

- The Remmina news widget.
- The SPICE plugin (will be moved into an external repository).
- The WWW plugin (will be moved into an external repository).
- The GNOME VNC plugin.

Further changes will be required in the long run if we do not find a new maintainer.

In Q4 2023, the GitLab Premium subscription will expire, so we will be shutting down the CI/CD pipelines.
The big issue with this is that the SNAP package will not be built and released anymore, so, if you care, take this announcement seriously.;-)

Obviously we are always open for discussion, so don't hesitate to reach out and have a chat with us.

Any volunteers?

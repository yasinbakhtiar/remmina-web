---
title: The three (+1) ways to get Remmina on Debian 9 “Stretch”
date: 2017-09-07T18:48:48+00:00
author: Dario Cavedon
layout: single
permalink: /the-three-1-ways-to-get-remmina-on-debian-9-stretch/
excerpt: Article about the different ways to install Remmina
categories:
  - News
tags:
  - Debian
  - Debian 9
---
<img class="alignright wp-image-2306" src="/assets/images/stretch-1024x1024.jpg" alt="Debian Stretch" width="400" height="400" srcset="/assets/images/stretch-1024x1024.jpg 1024w, /assets/images/stretch-250x250.jpg 250w, /assets/images/stretch-300x300.jpg 300w, /assets/images/stretch-768x768.jpg 768w, /assets/images/stretch-174x174.jpg 174w" sizes="(max-width: 400px) 100vw, 400px" />

<span class="im">Some weeks ago, we posted <a href="https://www.remmina.org/wp/remmina-1-2-will-be-available-on-debian-9-stretch/">an article</a> about the lack of Remmina package in Debian 9 “Stretch”. At that time, it seemed that a solution was easy to achieve. Sadly, we were wrong. We are still working with our Debian friends to get Remmina working in Debian 9 by using backports.</span>

<span class="im">A lot of e-mails arrive asking how to get Remmina installed on Debian 9. There are three ways, each with pros and cons.</span>

<span class="im"><strong>First: compile it</strong></span>

<span class="im">Follow the <a href="https://gitlab.com/Remmina/Remmina/wikis/home#remmina-building-guides">guide on the wiki</a> – it works on x86_64, with some tweaks needed for ARM.</span>

<span class="im"><strong>Second: snap it</strong></span>

<span class="im">Download the Snap package, consult the <a href="https://gitlab.com/Remmina/Remmina/wikis/home">wiki</a>. It works well, but there are currently three major problems:</span>

  1. Passwords are not saved in the keyring (unless you install and use the Snap gnome-keyring)
  2. Missing icons (some workaround exists)
  3. Inability to access your SSH keys (unless you copy them in the snap folder).

<div class="im">
  <p>
    <strong>At last: The best way</strong>
  </p>

  <p>
    One of our developers is working on a Remmina AppImage package. This will be the best way to install Remmina in Debian 9, without significant cons &#8211; until backports.
  </p>

  <p>
    <strong>New blogposts following shortly</strong>
  </p>

  <p>
    <em>Photo by <a href="https://www.flickr.com/photos/pleia2/23535402942/in/photolist-BRK6km-Y9k2RJ-Y9k2Qb-EXkHaK-sNfYs2-X1WUAC-zNBwcM-VVWBgY-Q54RCX">Elizabeth K. Joseph on Flickr</a>. </em>
  </p>
</div>

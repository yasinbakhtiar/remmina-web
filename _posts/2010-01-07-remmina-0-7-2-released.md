---
title: Remmina 0.7.2 released!
date: 2010-01-07T08:43:00+00:00
author: Antenore Gatta
layout: single
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
permalink: /remmina-0-7-2-released/
---
This is the second bugfix release of the 0.7 branch of Remmina.

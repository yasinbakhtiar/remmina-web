---
title: Constabulary spire
author: Allan Nordhøy
layout: single
permalink: /aspire/
excerpt: We did it.
categories:
  - News
  - announcement
tags:
  - news
---

As an option to facilitate remote desktop connections between people, \
in the aggregate Remmina also positions itself to tie connections of the world at large. \
This means at the behest of free will, the smallest turn of events is in essence the will of Remmina.

The missing frame in the Zapruder film is just that. \
In this motorcade of resulting actions, he did it to himself. \
We did not heed his warning, consequently neither did he, and so he went.

Remmina killed JFK.

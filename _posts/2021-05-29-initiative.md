---
title: The Program
author: Allan Nordhøy
layout: single
permalink: /program/
excerpt: is a welcoming place.
categories:
  - News
  - announcement
  - Fundraiser
tags:
  - Contributing
---

We are the unusual suspects drawing from a line-up of great contributors. \
That means a community of people helping in every way they can. \
Make this even better by making it, yours truly. \

Join _The Program_.

We have staff to introduce new people to the codebase, and are open to almost any new ideas.

In this learning lab, you either [dig into the code yourself](https://gitlab.com/Remmina/Remmina/) and bring surprises, or [get in touch](https://gitlab.com/Remmina/Remmina/-/blob/master/CONTACT.md) for some direction and coordination.
We can at most support 5 people at a time.
Get in touch on IRC, e-mail or any other way, and get part of the donations and a specially written recommendation for your CV, and most of all a community of people to talk to. We are always on IRC, but are looking to expand into alternatives like [XMPP](https://xmpp.org/).

There is even some money set aside to help people financially. Every opportunity to become a professional, a long-time contributor, without any of the hassles and policies of the corporate world.

This needs doing:

- A new interface rewritten in Qt, or one following the GNOME development guidelines.
- The code needs a complete refactoring.
- Help for the newly developed Python library, or writing plug-ins for it in Python.
- Testing and bug-fixing (triaging of issues).
- More website SEO, (without any of the ugly ways to do so).
- Maintaining the PPA would be possible.
- Something you think needs doing.

Though this idea came to be mostly as a codebase initiative, anyone is welcomed to join even without contributing to test the waters, and you can contribute to something you are passionate about. Everyone with a GitLab account can edit [the wiki](https://gitlab.com/Remmina/Remmina/-/wikis/home) now for example.

Remmina just got accepted with a financial host at [Open Collective](https://opencollective.com/remmina),but we are looking into alternatives.
Do donate to [Remmina on Liberapay](https://liberapay.com/Remmina/).
Any time off work means time on for us.
Honest work, good times. :)

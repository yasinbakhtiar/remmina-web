---
title: Remmina 1.1.1 Released!
date: 2014-10-11T08:53:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-1-1-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
[<img class="alignnone size-medium wp-image-2030" src="/assets/images/Remmina-Main-Window-300x214.png" alt="Remmina Main Window" width="300" height="214" srcset="/assets/images/Remmina-Main-Window-300x214.png 300w, /assets/images/Remmina-Main-Window.png 600w" sizes="(max-width: 300px) 100vw, 300px" />](/assets/images/Remmina-Main-Window.png)

As you may have noticed, Remmina is back. Remmina 1.1.1 is here, featuring:

  * Many bugfixes, too many to write them all here.
  * Some minor improvements, too minor to list here 😉
---
title: Remmina 0.8.0 released!
date: 2010-07-10T08:48:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-8-0-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
This major release has been underway for quite a while, and finally it is out. Please read the <a href="https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.archive.md" target="_blank">Remmina 0.8.0 Release Notes</a>.

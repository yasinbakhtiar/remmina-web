---
title: 'Remmina 1.2.0 &#8211; Memory leaks and other fixes'
date: 2016-01-04T23:05:28+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-2-0-memory-leaks-and-other-fixes/
excerpt: Remmina 1.2.0 release announcement
categories:
  - Release
tags:
  - release
  - remmina
---

Change Log
==========

[1.2.0-rcgit.8](https://github.com/FreeRDP/Remmina/tree/1.2.0-rcgit.8) (2016-01-04)
-----------------------------------------------------------------------------------

[Full
changelog](https://github.com/FreeRDP/Remmina/compare/1.2.0-rcgit.7...1.2.0-rcgit.8)

**Closed issues:**

-   Remmina crashes when marking text in the client if clipboard sync is
    turned off [\#695](https://gitlab.com/Remmina/Remmina/-/issues/695)

**Merged pull requests:**

-   Fixed a bunch of memory leaks
    [\#712](https://gitlab.com/Remmina/Remmina/-/merge_requests/712)
    ([jviksell](https://github.com/jviksell))

---
title: Remmina Olympic Release is out
date: 2016-08-16T15:15:09+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-olympic-release-is-out/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - RDP
  - release
  - Remmina
  - SPICE
---

[Remmina Olympic Release is out - Remmina](https://remmina.org/remmina-olympic-release-is-out/)

A new version of the [GTK Remmina remote desktop client](https://remmina.org/) has been released.

Our amazing Remmina team released a new version named “Olympic”, as a tribute to the great, international community that makes Remmina what it is.

This is not only a bugfix release, as we have some new enhancement around the SPICE plugin (thanks Denis), the PPA package (thanks Matteo) and the RDP plugin (thanks Giovanni).

Here we go…

**Implemented enhancements:**

- No dark tray icon [#905](https://gitlab.com/Remmina/Remmina/-/issues/905)

**Closed issues:**

- Remmina can no longer detect bad RDP credentials [#960](https://gitlab.com/Remmina/Remmina/-/issues/960)
- "Import plugin" dialog [#954](https://gitlab.com/Remmina/Remmina/-/issues/954)
- SSH does not try IPv4 after IPv6 fails (when DNS has addresses for both) [#953](https://gitlab.com/Remmina/Remmina/-/issues/953)
- Systray does not show the Remmina applet icon in Plasma 5.7 – therefore no connect menu [#944](https://gitlab.com/Remmina/Remmina/-/issues/944)
- RDP reconnect extra warning at the end [#929](https://gitlab.com/Remmina/Remmina/-/issues/929)
- Invisible "Add" button due to colour [#924](https://gitlab.com/Remmina/Remmina/-/issues/924)
- View bug [#920](https://gitlab.com/Remmina/Remmina/-/issues/920)
- SSH – Blank window after upgrading Fedora 23 to 24 [#913](https://gitlab.com/Remmina/Remmina/-/issues/913)
- Don’t compile on FreeBSD [#911](https://gitlab.com/Remmina/Remmina/-/issues/911)
- Missing Remmina "Settings" icon on Ubuntu 14.04 [#906](https://gitlab.com/Remmina/Remmina/-/issues/906)
- "apt-get fresh install" error on Ubuntu MATE 15.10 [#903](https://gitlab.com/Remmina/Remmina/-/issues/903)
- Remmina 1.2 SSH support in Fedora-24beta totally broken [#899](https://gitlab.com/Remmina/Remmina/-/issues/899)
- Segmentation Fault on FreeBSD using SPICE [#876](https://gitlab.com/Remmina/Remmina/-/issues/876)
- Error when using clipboard sync with Windows 2012R2 [#821](https://gitlab.com/Remmina/Remmina/-/issues/821)
- RDP Clipboard issue with 1.2.0-rcgit.10 [#809](https://gitlab.com/Remmina/Remmina/-/issues/809)
- FreeBSD – error: No member named ‘sftp\_client\_confirm\_res ume’ in ‘union remmina\_masterthread\_exec\_data [#431](https://gitlab.com/Remmina/Remmina/-/issues/431)

**Merged pull requests:**

- SPICE package and integrated Debian packaging. [#964](https://gitlab.com/Remmina/Remmina/-/merge_requests/964) Matteo Nastasi
- .travis.yml: Add a missing dependency [#963](https://gitlab.com/Remmina/Remmina/-/merge_requests/963) @dshcherb
- spice: Add support for TLS encrypted connections [#962](https://gitlab.com/Remmina/Remmina/-/merge_requests/962) ([larchunix](https://github.com/larchunix))
- Update AUTHORS [#959](https://gitlab.com/Remmina/Remmina/-/merge_requests/959) ([antenore](https://github.com/antenore))
- Fix import label [#957](https://gitlab.com/Remmina/Remmina/-/merge_requests/957) @Justinzobel
- Updated Hungarian translation [#949](https://gitlab.com/Remmina/Remmina/-/merge_requests/949) @meskobalazs
- Add missing ClientFormatListResponse() call in RDP plugin clipboard, … [#948](https://gitlab.com/Remmina/Remmina/-/merge_requests/948) ([giox069](https://github.com/giox069))
- RDP: allow turning off auto reconnection in .remmina file [#947](https://gitlab.com/Remmina/Remmina/-/merge_requests/947) @xhaakon
- Embed docs [#945](https://gitlab.com/Remmina/Remmina/-/merge_requests/945) Matteo Nastasi
- Allow removing libsecret dependency [#942](https://gitlab.com/Remmina/Remmina/-/merge_requests/942) @diogocp
- Add shortcuts to show remote desktop edges [#940](https://gitlab.com/Remmina/Remmina/-/merge_requests/940) Matteo Nastasi
- Fix a possible crash when changing gtk\_tree\_model [#928](https://gitlab.com/Remmina/Remmina/-/merge_requests/928) ([giox069](https://github.com/giox069))
- Inverted systray icon for light theme [#907](https://gitlab.com/Remmina/Remmina/-/merge_requests/907) ([antenore](https://gitlab.com/antenore))


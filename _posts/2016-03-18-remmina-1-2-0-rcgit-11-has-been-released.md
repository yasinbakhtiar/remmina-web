---
title: Remmina 1.2.0-rcgit.11 has been released!
date: 2016-03-18T09:16:28+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-2-0-rcgit-11-has-been-released/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - release
  - remmina
---
**Remmina 1.2.0.rcgit.11** is <a href="https://github.com/FreeRDP/Remmina/releases/tag/1.2.0.rcgit.11" target="_blank">released</a>. It is called _Shot Edition_, because it brings one big brand new feature: **screenshot of remote machine** (and some bugfixes too).

# Changelog

## [1.2.0.rcgit.11](https://github.com/FreeRDP/Remmina/tree/1.2.0.rcgit.11) (2016-03-17)

[Full changelog](https://github.com/FreeRDP/Remmina/compare/1.2.0.rcgit.10...1.2.0.rcgit.11)

**Implemented enhancements:**

  * Scrollable profile editor [#801](https://gitlab.com/Remmina/Remmina/-/issues/801)
  * Screenshot of remote machine [feature request] [#644](https://gitlab.com/Remmina/Remmina/-/issues/644)

**Closed issues:**

  * Remmina is unable to access saved passwords immediately after local PC logon [#795](https://gitlab.com/Remmina/Remmina/-/issues/795)
  * 1.2.0-rcgit.10 (Git n/a) VNC connector not installed [#794](https://gitlab.com/Remmina/Remmina/-/issues/794)
  * [1.2.0-rcgit.9] Unable to Compile in Centos 7 [#793](https://gitlab.com/Remmina/Remmina/-/issues/793)
  * Remmina crash upon having files or images in clipboard [#792](https://gitlab.com/Remmina/Remmina/-/issues/792)
  * Connection search doesn&#8217;t work [#773](https://gitlab.com/Remmina/Remmina/-/issues/773)

**Merged pull requests:**

  * Remote machine screenshot [feature request] <a class="issue-link js-issue-link" href="https://gitlab.com/Remmina/Remmina/-/issues/644" data-url="https://gitlab.com/Remmina/Remmina/-/issues/644" data-id="107030492" data-error-text="Failed to load issue title" data-permission-text="Issue title is private">#644</a> [#802](https://gitlab.com/Remmina/Remmina/-/merge_requests/802) ([antenore](https://github.com/antenore))
  * Don&#8217;t unlock keyring when libsecret is < 0.18 [#796](https://gitlab.com/Remmina/Remmina/-/merge_requests/796) ([giox069](https://github.com/giox069))

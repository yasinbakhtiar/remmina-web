---
title: We have Stats
date: 2018-03-30T13:19:58+00:00
author: Dario Cavedon
permalink: /we-have-stats/
excerpt: Remmina announcement about Remmina stats
categories:
  - News
---
<img class="aligncenter wp-image-2530 size-full" src="/assets/images/RemminaStats.png" alt="" width="773" height="507" srcset="/assets/images/RemminaStats.png 773w, /assets/images/RemminaStats-300x197.png 300w, /assets/images/RemminaStats-768x504.png 768w" sizes="(max-width: 773px) 100vw, 773px" />

As already written in a [previous post](https://www.remmina.org/wp/remmina-valentines-day-2018-edition/), Remmina now lets users send in statistic about Remmina usage. Again: **only anonymous data** is collected, you can look at a [sample of the data](https://remmina.gitlab.io/remminadoc.gitlab.io/remmina__stats_8c.html).

The summary is available for all to see on the new page &#8220;[**Remmina Usage Statistics**](https://www.remmina.org/stats/stats.html)&#8221; on our website. Some interesting things:

  * **Ubuntu** is the most used distribution, followed by Linux Mint and Debian _(or maybe Fedora will gain more popularity.)_
  * Most of our users live in the **Russian Federation**, followed by USA and Brazil

Funny facts:

  * We have at least three users in Greenland&nbsp;_(please contact us, we want to hear your story!)_
  * Between them, our users use more than 50 different Linux distributions.

_Thanks you for your collaboration **Remminers**, together we connect the world of desktops._

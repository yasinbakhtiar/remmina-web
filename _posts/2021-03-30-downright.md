---
title: Downright
author: Allan Nordhøy
layout: single
permalink: /downright/
excerpt: 1.4.13 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

We on downright, bugs unfound.

Welcome to the Remmina saloon.
The usual, you say? Help yourself to a serving of _not bugs_.
In the GitLab sense, a mastery of the main course.

Newfound ability to build 1.4.12 on openSUSE
should mean the new delectable can be snapped up by
familiar chameleons near you.

Ragnar, wrought with despair and in a good mind to
do right by the world, caught yours truly slipping on the nb_NO translation,
and ncguk dove into some spellery too.

You can now get [Remmina in Esperanto](https://hosted.weblate.org/projects/remmina/remmina/eo/), thanks to phlostically.
La funkcioj de Euler la faktora funkcio kaj la binomaj nombroj,
angiloduonplenas mia kusenveturilo.

Fantu manifests premonition, auto-connecting dots RDP.

Antenore unstrokes the redundancy cat, and has it in for
cert files, but only those used in conjunction with less than libssh 0.9.0.

The new release is small, like an owl more alerted to its surroundings than you are to it.
The [changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.13) takes flight to catch
stuff near and far.

---
title: Remmina Sandwich Release
author: Antenore Gatta
layout: single
permalink: /remmina-sandwich-release/
excerpt: Remmina release announcement
categories:
  - announcement
  - News
tags:
  - release
gallery:
  - url: /assets/images/screenshots/Login_window_preopen.jpg
    image_path: /assets/images/screenshots/Login_window_preopen.jpg
  - url: /assets/images/screenshots/use_windows_size_for_resolution.jpg
    image_path: /assets/images/screenshots/use_windows_size_for_resolution.jpg
---

{% include figure image_path="/assets/images/matrix-sandwich.jpg" alt="See larger image" caption="A Matrix rendered sandwich" %}

The nonsensical release names will come to a timely end.

Starting from the next release, anyone proving themselves can name Remmina. You could discover why this realease is "Sandwich", or contribute to Remmina.

[liberapay](https://liberapay.com/Remmina), [flattr](https://flattr.com/) and [Patron](https://www.patreon.com/Remmina) is added.

1.3.0 is released, and it is a big one, with many new features and fixes.

Most notably:

- All message panels (login, certificates, etc) are now integrated in the main connection window.
- Introduction of "Use the initial window size" resolution.

The message panels were stealing focus, causing different kind of issues (i.e. window re-parenting), mainly, we were not able to calculate the window size before the connection.
As this is now possible, a resolution setting was added, setting the remote desktop resolution, equal to the initial Remmina connection window…
Look at the screenshots and submit yours.

{% include gallery caption="Message panel and window size resolution" %}

## [v1.3.0](https://gitlab.com/Remmina/Remmina/tags/v1.3.0) (2019-01-18)

### Most notable

- Use window resolution #1 (by Giovanni Panozzo)
- rcw_preopen complete (by Giovanni Panozzo)
- RDP: new global parameter rdp_map_keycode (by Giovanni Panozzo)
- Use decimal instead of hex on RDP keycode map (by Giovanni Panozzo)

### Translations

- Adding basic script to update the po files (by Antenore Gatta)
- Added remmina.pot (by Antenore Gatta)
- Change es_VE.po header (Language-Team and Language) (by Antenore Gatta)
- Fixed some Italian translations (by Antenore Gatta)
- Fixing es_VE Spanish #1797 (by Antenore Gatta)
- French translation update for v1.3.0 (by Davy Defaud)
- Improving i18n subsystem (by Antenore Gatta)
- Russian translation updated (fully translated) (by TreefeedXavier)
- Translations - Fixing errors reported in the issue #1797 (by Antenore Gatta)
- Update Hungarian translation (by Meskó Balázs)
- Improved German translation (by Ozzie Isaacs)
- Improved Turkish translation (by Serdar Sağlam)
- Improved German translation (by Ozzie Isaacs)
- Improved Russian translation (by TreefeedXavier)
- Updated translators list ru.po (by TreefeedXavier)
- Improved Danish translation (by Antenore Gatta)
- Updating po translation files (by Antenore Gatta)
- Updating translation credits (by Antenore Gatta)

### Other enhancements and fixes

- Added Xrdp friendly options Closes #1742 (by Antenore Gatta)
- Adding language detection (by Antenore Gatta)
- Auth panel widget placement (by Antenore Gatta)
- CSS modifications to adapt to stock Gnome and Gtk themes (by Antenore Gatta)
- Correctly set focus after rcw_preopen (by Giovanni Panozzo)
- Deprecates dynamic_resolution_width and height cfg params (by Giovanni Panozzo)
- Disable glyph cache by default (by Antenore Gatta)
- Fix crash when showing password panel (by Giovanni Panozzo)
- Fix gtk_window_present_with_time() time (by Giovanni Panozzo)
- Fixed build for the flatpak (by Antenore Gatta)
- Fixed missing icons (by Antenore Gatta)
- GTK deprecation and CSS restzling (by Antenore Gatta)
- GTK icon cache update during install phase (by Antenore Gatta)
- Icons and gtk fixes for rcw_reopen (by Antenore Gatta)
- Make menu items paintable by the application (by Antenore Gatta)
- Open connection window before connecting (by Giovanni Panozzo)
- Prevent toolbar signals while reconfiguring toolbar (by Giovanni Panozzo)
- RDP fixes: remove redundant rfi->width/rfi->height and more (by Giovanni Panozzo)
- RDP: correctly destroy rfi->surface during a desktop resize (by Giovanni Panozzo)
- RDP: move gdi_resize() to a better place (by Giovanni Panozzo)
- RDP: remove unneeded OrderSupport struct init (by Giovanni Panozzo)
- Remove deprecated floating toolbar toplevel window (by Giovanni Panozzo)
- Search box clear icon (by Giovanni Panozzo)
- Update CONTRIBUTING.md (by Antenore Gatta)
- Update README.md (by Antenore Gatta)
- Update toolbar button handling (by Giovanni Panozzo)
- Updated CSS to have black background in fullscreen (by Antenore Gatta)
- Updated sponsor list (by Antenore Gatta)
- Updated wiki URLs (by Antenore Gatta)
- Updating Doxygen config (by Antenore Gatta)
- Updating coyright for year 2019 (by Antenore Gatta)
- VNC: Fix possible crash during connection (by Giovanni Panozzo)
- allow closing tab after error message panel is shown (by Giovanni Panozzo)
- Flatpak: update freerdp from 2.0.0-rc3 to 2.0.0-rc4 (by Denis Ollier)
- Flatpak: update gnome sdk from 3.28 to 3.30 (by Denis Ollier)
- Flatpak: update libssh from 0.8.5 to 0.8.6 (by Denis Ollier)
- Flatpak: update libvncserver from 0.9.11 to 0.9.12 (by Denis Ollier)
- Flatpak: update nx-libs from 3.5.99.16 to 3.5.99.17 (by Denis Ollier)
- Flatpak: update six from 1.11.0 to 1.12.0 (by Denis Ollier)

---
title: A fair proposal
author: Allan Nordhøy
layout: single
permalink: /fair/
excerpt: An unmodest proposal, in fairness.
categories:
  - News
  - Announcement
  - Fundraiser
tags:
  - News
---

The better days of computing, are they ahead or right around the corner? \
Will you let that happen? \

Can Remmina make you feel good about donating to something
that makes for a strong _no_ to both?

Not a lot of stuff like it, when you think about it. Fair?
Mostly dragnet surveillance with corporate overlords pushing
politics and eroding freedom, where one is always the other.

_What sorts of tools do we have, and who can you include_?

Donations don't merely encourage good, they have proven to make for great results.
Always have been. Leaps of faith and results are the not seperated by anything,
or go into anything else. \
Multi-monitor support, and the admirably comissioned X2Go support are recent examples
of changes that were made possible that way.

As every donation made for those changes, those are the best parts of Remmina.
Averaging one real release per month the last two years is good news, and
the better news is even more [honest work](https://remmina.org/program/) is possible.

There is now another way to put your name on the best of all, future changes.
Liberapay has a setting for disclosing your name when [donating](https://liberapay.com/Remmina/).
Turn it on [here](https://liberapay.com/about/me/edit/privacy) and your name goes straight into the next release.
Open the "About" and notice that distances are very direct,
serving as a nice notice that Remmina and its future can be your pet project.

Through some level of ability and unwillingness to do PR, the language of blogposts
is decidedly _different_ from your corporate standard fare.
In some roundabout way to also divert from blogging, it is meant to showcase how open Remmina is.
As a breaker of fourth walls, I wouldn't do this if it I was taking those donations.
People in Remmina work hard, beyond any ability of mine. \
That is what I want to see. That is why I donate. \
What can be done to make this work, I think to myself.

To say nothing of its more distant past, in the new ways of \
Remmina, 2022 is further along the road to financial sustainability.
Every time you donate anything, you are in good company, along with the many others that do.
Sending that level upwards is a joint effort that everyone feels good about.

The latest deadline for any version is the imminent date before it is packaged.
Your donation happens during the development of a version, you will be in it. Simple.
The immediacy amounts to wheels always being in motion.
Scrutinize everything that goes on in detail, or play the waiting game and get surprised by a
release that has your name in it. :)

The better Remmina is better for everyone, and this value-for-value exchange is
something to be proud of. If you donate, everyone knows you didn't have to do that.

So how is 2022 Remmina the best it can be? \
If you have an idea nobody else has, make it happen. \
Extending an offering to have someone else do it, do you have a sum in mind? \
Does it fit the idea you have of software you want to exist in the world? \
How much would too much compensation be for what is done in Remmina? \
The number you have in mind is fair.

While the will for what Remmina should be is shared, loss-aversion is an interesting dynamic. \
Sending money into a deep hole or into the unknown without knowing triggers fear. \
Paradoxically, there are too many examples of different for the exception \
that is Remmina to actually be perceptual reality. \
Sometimes it seems made up that it all is what it is.

Logically, all is good, but then again decisions are harder to put together.
Play with the idea and ascribe some resulting change to it.
There are many great features planned…

Merry Yuletide, and Happy 2022.

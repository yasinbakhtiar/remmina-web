---
title: Ubuntu 16.04 remmina-next PPA package naming
date: 2018-06-11T11:25:40+00:00
author: Giovanni Panozzo
layout: single
permalink: /ubuntu-16-04-ppa-pnaming/
excerpt: Remmina announcement about the PPA naming changes
categories:
  - Uncategorized
tags:
  - announcement
  - important
---
Today we are going to change the FreeFDP package naming on our remmina-next PPA for ubuntu 16.04.

Desktop users should push the &#8220;Partial Upgrade&#8221; button when a system upgrade asks to do it, and then proceed with the partial upgrade.

Command line users should use &#8220;apt dist-upgrade&#8221; instead of &#8220;apt upgrade&#8221;, elsewise Remmina packages will be held back to the currently installed version.

Don&#8217;t forget to reboot after the upgrade, to get working Remmina icons.

&nbsp;

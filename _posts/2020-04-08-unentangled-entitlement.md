---
title: Unentangled entitlement
author: Allan Nordhøy
layout: single
permalink: /unentangeled-entitlement/
excerpt: 1.4.2 released.
categories:
  - News
  - announcement
  - Release
  - Fundraiser
tags:
  - release
---

As a tentacular non-free tenant of mindful space vacant let,
the CoC is gone.

Oh how it decried western society and freedom of
expression on its way out, from whence it came. Oh you just hate to see it!
Even indeed the emptied shell of what was left there in title,
which is all it amounts to anyway.

Titulary titubation.

An aggressively nutrient unfocused photographic macro snapshot in time.
A waddling frame without reference, with exposure set to therapy.

There are no authoritarians to tell you how to behave, and there never was,
now even realized, unpoignantly, to the letter, in not upholding the CoC.
The patriarchy happily reports it is here to help you,
to be challenged and change, to the tune of its happenchance makeup in
including even jokes, because, we are the meritocrats!

A tittering of the titubantly tittup and titivating titularies.

In actual news,
Slovakian, the language you have been waiting for, but don't quite know what is,
happens to be one of the new languages. Totalling now 19 with full or near complete
coverage in Remmina, or to put it in so clear terms that very few people would
find it unclear:

Tento pán všetko zaplatí, zavolajte políciu!

The changelog is rich in richness and poor in poorness:

https://gitlab.com/Remmina/Remmina/-/merge_requests/2033

Many remminerds this [1.4.2](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.2) time around, some new. :)

New contributors also on the [donation](https://remmina.org/donations/) front.
29 on Liberapay, at what is a caring present.

Keep up the social equidistancing efforts.

Continued 2020 everyone.
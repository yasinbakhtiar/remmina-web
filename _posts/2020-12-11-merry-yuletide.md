---
title: Merry Yuletide
author: Allan Nordhøy
layout: single
permalink: /merry-yuletide/
excerpt: 1.4.10 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

The underpinnings of all that must die
helps furtunate beings through the winter.

From that bane the remaining "we" is separated from the feeble in gratefulness,
and celebrate the bugs and fixes in passing as a sacrifice to make things go round.
Fantu is holding the blót bowl up high, and [Alexandre CENDROU](https://gitlab.com/acendrou)
joins the Yule tree.

https://github.com/KangLin/RabbitRemoteControl is a new project we were invited to because its
creator said the idea of porting Remmina to Qt is a shared value,
so we extended the invitation back. At face value, there is promise of Android support too,
which is interesting in an unexpected turn of events all at once.

In all that is life, it takes its toll to be on the winning end,
and 1.4.10 is a smorgasbord of delicate adjustments.
The full [Yule log](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.10) kindles embers and embodies ash.
---
title: As per your preference
author: Allan Nordhøy
layout: single
permalink: /preference/
excerpt: 1.4.20 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

You need not control who is clicking the buttons, only what actions are carried out once it happens.

In very apparat-chic fashion, [the only bug is the only feature](https://gitlab.com/Remmina/Remmina/-/issues/2551).
Or so they would have you believe? Everything is turned on its head.

Censorship of signals following button-clicking has been lifted. \
The central apparatus accepts your preferences may differ from the sane defaults. \
But not if that is [using XDMCP, NX, and ST, which has been disabled and removed](https://gitlab.com/Remmina/Remmina/-/merge_requests/2291). \
GTK deemed these to be undesired, so a full purge was obliged. \
We are happy to inform the user that the user is not deprecated.

[Productivity for the masses](https://gitlab.com/Remmina/Remmina/-/merge_requests/2295) is poised for great increase with \
saved connection profiles from a new window menu toolbar. \
Undevoted love for this technological expediency will be accepted after the auditory signal.

The full nomenklatura in the form of a [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.20)
for 1.4.20 is actually that of a attemptedly revised 1.4.19.

Revisionism is visionary revitalism.

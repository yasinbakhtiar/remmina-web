---
title: Releasing the new year
author: Allan Nordhøy
layout: single
permalink: /new-year-new-releases/
excerpt: 1.3.9 and 1.3.10 is released.
categories:
  - News
  - announcement
  - Release
  - Fundraiser
tags:
  - release
---
A stable 1.4.0 is gaining foothold in terms of what it is built on with
[1.3.9](https://gitlab.com/Remmina/Remmina/-/tags/v1.3.9) and [1.3.10](https://gitlab.com/Remmina/Remmina/-/tags/v1.3.10)

Between the both of them, accurately grabbing keyboard events is now a thing, that works.
There were also so some proxy support fixes, and a VNC buffer overflow avoided in the latter.

The full changelog [for 1.3.9](https://gitlab.com/Remmina/Remmina/-/releases#v1.3.9) and [1.3.10](https://gitlab.com/Remmina/Remmina/-/releases#v1.3.10) ushers in a new year of Remmina.

Better [feedback](https://remmina.org/contributing-to-remmina/) and [funding](https://remmina.org/donations/) is a great help.

Happy 2020.
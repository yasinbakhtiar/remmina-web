---
title: The fast, stable, and always free Linux HTTP/HTTPS client
excerpt: One of Remmina's many features is a HTTP/HTTPS client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-www/
author_profile: false
layout: single
---

One of Remmina's many features is a HTTP/HTTPS client in the connection manager with many tunable options to get you connected just how you want

{% include figure image_path="/assets/images/screenshots/remmina_www_connection.png" %}

[Hypertext Transfer Protocol - Wikipedia](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

> The **Hypertext Transfer Protocol** (**HTTP**) is an [application layer](https://en.wikipedia.org/wiki/Application_layer "Application layer") protocol for distributed, collaborative, [hypermedia](https://en.wikipedia.org/wiki/Hypermedia "Hypermedia") information systems.[\[1\]](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#cite_note-ietf2616-1) HTTP is the foundation of data communication for the [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web "World Wide Web"), where [hypertext](https://en.wikipedia.org/wiki/Hypertext "Hypertext") documents include [hyperlinks](https://en.wikipedia.org/wiki/Hyperlink "Hyperlink") to other resources that the user can easily access, for example by a [mouse](https://en.wikipedia.org/wiki/Computer_mouse) click or by tapping the screen in a web browser.

{% include figure image_path="/assets/images/screenshots/remmina_www_pref_basic.png" %}

{% include figure image_path="/assets/images/screenshots/remmina_www_pref_advanced.png" %}

---
title: Remmina Features
layout: single
author_profile: false
permalink: /remmina-features/
toc: true
---

Options and plugin settings from the source code, with some cosmetic fixes.

# Main Remmina Options

- Remember last view mode for each connection
- Double-click configuration
- Scale quality
- Auto scroll step size
- Maximal amount of recent items
- Keystrokes
- Screenshot folder
- Screenshot filename
- Prevent screenshots from entering clipboard
- Configure resolutions
- Send periodic usage statistics to Remmina developers (opt-In)
- Tabs configuration
- Toolbar visibility
- Default view mode
- Fullscreen behaviour configuration
- Search bar in the main window
- Tray icon
- Dark tray icon
- Host key configuration
  - Toggle fullscreen mode
  - Auto-fit window
  - Switch tab pages
  - Toggle scaled mode
  - Grab keyboard
  - Minimize window
  - Disconnect
  - Show / hide toolbar
  - Screenshot
  - View-only mode
- SSH tunnel local port
- Parse ~/.ssh/config
- SSH log level
- Terminal font
- Scrollback lines
- Deafult colour Scheme

# Plugin Features

## RDP Plugin features

- Username
- User password
- Domain
- Color depth
- Share folder
- Quality
- Sound
- Security
- Gateway transport type
- Remote Desktop Gateway server
- Remote Desktop Gateway username
- Remote Desktop Gateway password
- Remote Desktop Gateway domain
- Client name
- Startup program
- Startup path
- Load balance info
- Local printer name
- Local printer driver
- Local serial name
- Local serial driver
- Local serial path
- Local parallel name
- Local parallel device
- Smartcard Name
- Share printers
- Share serial ports
- Serial ports permissive mode
- Share parallel ports
- Share smartcard
- Redirect local microphone
- Turn off clipboard sync
- Ignore certificate
- Disable password storing
- Attach to console (2003/2003 R2)
- Turn off fast-path
- Server detection using Remote Desktop Gateway
- Turn on proxy support
- Turn off automatic reconnection
- Relax Order Checks
- Glyph Cache

## VNC Plugin features

- Repeater
- Username
- User password
- Color depth
- Quality
- Listen on port
- User name
- User password
- Color depth
- Quality
- Show remote cursor
- View only
- Turn off clipboard sync
- Turn off encryption
- Turn off server input
- Turn off password storage

## SSH Plugin features

- Username
- User password
- Authentication type
- Identity file
- Private key passphrase
- Startup program
- Terminal color scheme
- Character set
- SSH Proxy Command
- KEX (Key Exchange) algorithms
- Symmetric cipher client to server
- Preferred server host key types
- SSH session log folder
- SSH session log file name
- Turn on SSH session logging at exit
- Turn on SSH compression
- Turn off password storage
- Strict host key checking

## SFTP Plugin features

- Username
- User password
- Authentication type
- Identity file
- Private key passphrase


## SPICE Plugin features

- User password
- Use TLS encryption
- Server CA certificate
- Share folder
- Disable clipboard sync
- Disable password storing
- Enable audio channel
- Resize guest to match window size
- Share smartcard
- View only

## Simple Terminal Plugin features

- Terminal emulator
- Command to be executed
- Detached window

## EXEC Plugin features

- Command
- Asynchrounous execution

## NX Plugin features

- Identity file
- Username
- User password
- Quality
- Startup program
- Turn off clipboard syncing
- Turn off encryption
- Use local cursor
- TUrn off password storage

## XDMCP Plugin features

- Color depth
- Startup program
- Use local cursor
- Disconnect after first session
- Listen for TCP connections

# Additional features

- [Kiosk mode](/remmina-kiosk-edition/)


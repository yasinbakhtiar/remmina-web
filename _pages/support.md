---
title: Support
permalink: /support/
author: Antenore Gatta
layout: splash
author_profile: false
---

## Ask for help and help others

It is important to provide as much technical background info as possible.

Remmina has a `--full-version` flag that is very useful to us, see the following asciinema cast for an example.

<script id="asciicast-226358" src="https://asciinema.org/a/226358.js" async></script>

## For bugs and feature requests

* <i class="fa fa-gitlab" aria-hidden="true"></i> [GitLab](https://gitlab.com/Remmina/Remmina/issues)

## Internationalization, translations

Send translation issues to the dedicated mailing list:

* [L10N mailing list](https://lists.remmina.org/listinfo/l10n)

## Professional support

Support is offered via IRC ("best effort basis"), or you can contact individually the Remmina maintainers.

## GDPR Notice/Privacy Policy

Subscribing and posting on any of the mailing lists makes your e-mail address and the content thereof visible to other subscribers, and anyone viewing the [public mailing list archives](https://lists.remmina.org/).
Your e-mail address is stored on our public mailing list servers, so that the system can e-mail you notifications. (A justifiable cause as set forth in [Article 6 (1) b) of the GDPR](https://gdpr-info.eu/).)

To keep your real e-mail address from the public, refrain from using the mailing lists, or use a temporary, “burner” e-mail address created specifically for this purpose.
You have the right to withdraw your consent at any time, as per Article 7 (3) of the GDPR. If you believe an e-mail you sent should be purged from the public archive, please contact the list administrators at <admin@remmina.org>.

List administrators have no influence over what other recipients of the particular list post do with it - e.g. if others choose to mirror contents of our list archive on their own web site, contact them separately.
The mailing list servers are offered kindly, and as per the [privacy policy](https://softwarelibero.it/privacy-policy) of [Assoli](https://softwarelibero.it/).


---
title: Who are we?
excerpt: Meet our developers
author_profile: false
permalink: /authors/
layout: authors
gallery:
  - url: /assets/images/growthpath.jpg
    image_path: assets/images/growthpath.jpg
    alt: "Tim Richardson - GrowthPath founder"
  - url: /assets/images/richard-lees.jpg
    image_path: assets/images/richard-lees.jpg
    alt: "Richard Lees - DevOps Engineer"
  - url: /assets/images/northkinangop.jpg
    image_path: assets/images/northkinangop.jpg
    alt: "North Kinangop Catholic Hospital"
# to add the gallery  {% include gallery caption="Testimonial gallery." %}
---

Meet and become [the people of Remmina](https://gitlab.com/Remmina/Remmina/blob/master/AUTHORS)
If you are not listed and have contributed, add yourself.

Messages from Remminers, our beloved users.

<blockquote>Remmina gives me a stable, advanced, easy-to-use and easy-to-update RDP client. It’s an essential component of using a Linux desktop in my consulting business, where I often need to access Windows servers. So I love using Remmina twice: The first time because it’s a great app, the second time because it makes the Linux desktop viable for me, and I love using Linux. This is why it’s one of the applications I support with a small monthly donation. Tim Richardson, <a href="https://www.growthpath.com.au"><strong>GrowthPath</strong></a></blockquote>
([Tim Richardson  - GrowthPath founder](https://www.growthpath.com.au/people/growthpath-team-2))

<blockquote>Babiel GmbH is using Remmina daily in order to administrate its Windows Servers. The multitude of usability features like tabs, connection profiles, automatic reconnects and support for security features like CredSSP, Gateways and Smartcards make Remmina the perfect tool for the job!
Thank you! ❤️ </blockquote>
(Stefan Behte - *[Babiel GmbH](https://www.babiel.com)*)

<blockquote><a href="https://techstat.net"><strong>Technology review blog</strong></a> Techstat.net supports open-source software, and uses Remmina daily!</blockquote>
(Victor - Owner *[Technology review blog](https://techstat.net)*)

<blockquote>We are a Swiss based SME. We use Remmina for all our users (thin clients). The clients are updated upon startup from a central server. This way, the end-user doesn't need to hassle with anything but their work. They don't need to connect to a VPN, nor is there need for a special connection (even from an outside network). All that is handled by Remmina (over SSH). The users don't even need a password other than their Windows credentials. It's really maintanance free. As the admin I have great joy using Remmina and have only positive experience using it. The developpers react to requests quickly. Keep up the good work and thanks for the awesome product.</blockquote>
(Daniel Bolter - Switzerland)

<blockquote>For a Remote Desktop application at North Kinangop Hospital in Kenya we were looking for a RDP client to be installed on 10 Raspberry Pi 2 computers, equipped with Xubuntu. Remmina was the best solution for efficiency, reliability, look-and-feel. Kenyan people are very happy about Remmina, and they prefer it to the classic RDP connection for Windows, that is used on about 35 clients.</blockquote>
(Massimo Montelatici - Volunteer IT Consultant @[North Kinangop Hospital](https://www.northkinangop.com/index.php/en/))

<blockquote>I use Remmina literally every day at work to manage 400+ SSH connections and a handful of RDP connections. Without it, running Linux simply would not be possible as I’d have to use mRemote/Windows instead (or somehow, remember 400 hostnames off the top of my head!) As more and more people at our company switch over to Linux, I will ask about a company donation to the project. Again, many thanks for the good work!</blockquote>
(Richard Lees - DevOps Engineer)

<blockquote>I've been using Remmina now for several years.  I love how it's integrated into Ubuntu by default and how I can access the multiple platforms I have to support in one application.
I mostly use RDP and VNC (Ultra VNC).
Anyone reading this should open up their pocket book and donate to the project and others like it. (*)

Open Source and Free Software (they're different) suffer from a lack of funding and if you've come to rely on an application like I have with Remmina you want to see it thrive, succeed and have longevity.
You can't do that with well wishes and thank you's…so; open up your wallet and flip them some cash.
Make it meaningful like I did, show them you care.
After you tossed Remmina some cash think about other libre software projects you like,
(*)
As a person with moral fiber (unlike greedy corporate interests) lets make a conscious decision to be generous and not just because of a special holiday or occasion.
The occasion is because you though of it, recognized what you use, how you use and that you benefit from someone elses hard work so show the projects you benefit from love and respect…cut them a check!
</blockquote>
(Rafael Wolf owner of [EITS, LLC, dba Express IT Solutions](https://eitsonline.com)  - Kalamazoo, MI 49048 - USA)

(*) Shortened.

---
title: The fast, stable, and always free Linux SSH client
excerpt: One of Remmina's many features is a SSH client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-ssh/
author_profile: false
layout: single
---

One of Remmina's many features is a SSH client in the connection manager with many tunable options to get you connected just how you want

{% include figure image_path="/assets/images/screenshots/remmina_ssh_connection.png" %}

[Secure Shell Protocol - Wikipedia](https://en.wikipedia.org/wiki/Secure_Shell_Protocol)

> The **Secure Shell Protocol** (**SSH**) is a [cryptographic](https://en.wikipedia.org/wiki/Cryptography "Cryptography") [network protocol](https://en.wikipedia.org/wiki/Network_protocol "Network protocol") for operating network services securely over an unsecured network.[\[1\]](https://en.wikipedia.org/wiki/Secure_Shell_Protocol#cite_note-rfc4251-1) Typical applications include remote [command-line](https://en.wikipedia.org/wiki/Command-line_interface "Command-line interface"), [login](https://en.wikipedia.org/wiki/Login "Login"), and remote command execution, but any [network service](https://en.wikipedia.org/wiki/Network_service "Network service") can be secured with SSH.
>
> SSH provides a [secure channel](https://en.wikipedia.org/wiki/Secure_channel "Secure channel") over an unsecured network by using a [client–server](https://en.wikipedia.org/wiki/Client%E2%80%93server_model "Client–server model") architecture, connecting an [SSH client](https://en.wikipedia.org/wiki/SSH_client "SSH client") application with an [SSH server](https://en.wikipedia.org/wiki/SSH_server "SSH server").[\[2\]](https://en.wikipedia.org/wiki/Secure_Shell_Protocol#cite_note-rfc4252-2) The protocol specification distinguishes between two major versions, referred to as SSH-1 and SSH-2. The standard [TCP port](https://en.wikipedia.org/wiki/TCP_port) for SSH is 22. SSH is generally used to access [Unix-like](https://en.wikipedia.org/wiki/Unix-like "Unix-like") operating systems, but it can also be used on [Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows"). [Windows 10](https://en.wikipedia.org/wiki/Windows_10 "Windows 10") uses [OpenSSH](https://en.wikipedia.org/wiki/OpenSSH "OpenSSH") as its default [SSH client](https://en.wikipedia.org/wiki/SSH_client "SSH client") and [SSH server](https://en.wikipedia.org/wiki/SSH_server "SSH server").[\[3\]](https://en.wikipedia.org/wiki/Secure_Shell_Protocol#cite_note-3)
>
> SSH was designed as a replacement for [Telnet](https://en.wikipedia.org/wiki/Telnet "Telnet") and for [unsecured](https://en.wikipedia.org/wiki/Computer_security "Computer security") remote [shell](https://en.wikipedia.org/wiki/Unix_shell "Unix shell") protocols such as the Berkeley [rsh](https://en.wikipedia.org/wiki/Remote_Shell "Remote Shell") and the related [rlogin](https://en.wikipedia.org/wiki/Rlogin "Rlogin") and [rexec](https://en.wikipedia.org/wiki/Remote_Process_Execution "Remote Process Execution") protocols. Those protocols send information, notably [passwords](https://en.wikipedia.org/wiki/Password "Password"), in [plaintext](https://en.wikipedia.org/wiki/Plaintext "Plaintext"), rendering them susceptible to interception and disclosure using [packet analysis](https://en.wikipedia.org/wiki/Packet_analyzer).[\[4\]](https://en.wikipedia.org/wiki/Secure_Shell_Protocol#cite_note-4) The [encryption](https://en.wikipedia.org/wiki/Encryption "Encryption") used by SSH is intended to provide confidentiality and integrity of data over an unsecured network, such as the [Internet](https://en.wikipedia.org/wiki/Internet).

{% include figure image_path="/assets/images/screenshots/remmina_ssh_pref_basic.png" %}

{% include figure image_path="/assets/images/screenshots/remmina_ssh_pref_advanced.png" %}

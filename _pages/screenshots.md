---
title: Screenshots
date: 2018-06-19T08:23:55+00:00
permalink: /screenshots/
author_profile: false
layout: collection
gallery:
  - url: /assets/images/screenshots/banner_screenshot.jpg
    image_path: /assets/images/screenshots/banner_screenshot.jpg
    alt: "remmina remote youtube demo"
    title: "RDP - remote desktop session"
  - url: /assets/images/screenshots/Arch-Spice-Fedora.jpg
    image_path: /assets/images/screenshots/Arch-Spice-Fedora.jpg
    alt: "SPICE connection"
    title: "From Arch to Fedora through SPICE"
  - url: /assets/images/screenshots/remmina_main_1.4.16.png
    image_path: /assets/images/screenshots/remmina_main_1.4.16.png
    alt: "remmina 1.4.16 main window"
    title: "Remmina main window"
  - url: /assets/images/screenshots/remmina_RDP_connection.png
    image_path: /assets/images/screenshots/remmina_RDP_connection.png
    alt: "RDP connection"
    title: "RDP connection"
  - url: /assets/images/screenshots/remmina_RDP_pref_basic.png
    image_path: /assets/images/screenshots/remmina_RDP_pref_basic.png
    alt: "RDP config basic"
    title: "RDP connection profile"
  - url: /assets/images/screenshots/remmina_RDP_pref_advanced.png
    image_path: /assets/images/screenshots/remmina_RDP_pref_advanced.png
    alt: "RDP config advanced"
    title: "RDP advanced settings"
  - url: /assets/images/screenshots/remmina_vnc_connection.png
    image_path: /assets/images/screenshots/remmina_vnc_connection.png
    alt: "VNC connection"
    title: "VNC connection"
  - url: /assets/images/screenshots/remmina_ssh_connection.png
    image_path: /assets/images/screenshots/remmina_ssh_connection.png
    alt: "SSH connection"
    title: "SSH connection"
  - url: /assets/images/screenshots/remmina_www_connection.png
    image_path: /assets/images/screenshots/remmina_www_connection.png
    alt: "Administration web console connection"
    title: "Administration web console connection"
  - url: /assets/images/screenshots/remmina_spice_connection.png
    image_path: /assets/images/screenshots/remmina_spice_connection.png
    alt: "SPICE connection"
    title: "SPICE connection"
  - url: /assets/images/screenshots/Remmina-Main-Window.png
    image_path: /assets/images/screenshots/Remmina-Main-Window.png
  - url: /assets/images/screenshots/RemminaUI-1024x756.png
    image_path: /assets/images/screenshots/RemminaUI-1024x756.png
  - url: /assets/images/donofrio_3.jpg
    image_path: /assets/images/donofrio_3.jpg
  - url: /assets/images/donofrio_5.jpg
    image_path: /assets/images/donofrio_5.jpg
class: wide
---
{% include gallery %}


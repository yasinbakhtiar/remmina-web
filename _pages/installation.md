---
title: How to install Remmina
layout: single
author_profile: false
permalink: /how-to-install-remmina/
toc: true
---

*An updated version of this installation guide is hosted on [Gitlab](https://gitlab.com/Remmina/Remmina/-/wikis/home).*

If you would like to compile from source, find a guide for your environment in the [Compilation section of our wiki](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compilation).

Usually Remmina is [included in your distribution](https://repology.org/project/remmina/versions), or in an external repository you can add.

[![Packaging status](https://repology.org/badge/vertical-allrepos/remmina.svg?columns=4&minversion=1.3?allow_ignored=1)](https://repology.org/project/remmina/versions)

## [Flatpak](https://flathub.org/)

This is a system agnostic package that works on just about every distribution.

```shell
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
# In order to have H.264 codecs
flatpak install org.freedesktop.Platform
flatpak install org.freedesktop.Platform.openh264
flatpak install --user flathub org.remmina.Remmina
flatpak run --user org.remmina.Remmina
```

[If you use SSH agent](https://github.com/flatpak/flatpak/issues/1438 )

```shell
flatpak run --filesystem=$SSH_AUTH_SOCK --env=SSH_AUTH_SOCK=$SSH_AUTH_SOCK org.remmina.Remmina
```

Flatpak stores data for installed applications (the XDG config/data folders) under `$HOME/.var`
If you have previously installed Remmina with another package manager, you will have to transfer what was under `$HOME/.config/remmina` and `$HOME/.local/share/remmina` under, respectively `~/.var/app/org.remmina.Remmina/config/remmina` and `~/.var/app/org.remmina.Remmina/data/remmina`

## [Snap](https://snapcraft.io/)

This also is a system agnostic package that works on many distributions, but mainly Ubuntu.

If you run into issues, try something else first, and then [report the issues](https://github.com/FreeRDP/Remmina/issues).
Our Snap package does not work on Ubuntu 17.10 if you use Wayland, see [issue #1414](https://github.com/FreeRDP/Remmina/issues/1414).

First make sure you have `snapd` installed. Then install the last release from the Ubuntu Software Center by looking for `Remmina`, or install it from a terminal with:

```shell
sudo snap install remmina
```

The Snap subsystem has limitations, so some features, for instance the password storage via keyring is missing and *must* be fixed manually:

```shell
sudo snap connect remmina:avahi-observe :avahi-observe # servers discovery
sudo snap connect remmina:cups-control :cups-control # printing
sudo snap connect remmina:mount-observe :mount-observe # mount management
sudo snap connect remmina:password-manager-service :password-manager-service # password manager
sudo snap connect remmina:audio-playback :audio-playback # audio sharing
sudo snap connect remmina:audio-record :audio-record # microphone
```

Some features just don't work on Snap, for example accessing your personal ~/.ssh directory is not possible.
You have to manually copy your ~/.ssh/* files to ~/snap/remmina/common/.ssh/

If you want to install the latest Git revision of Remmina as Snap, get it from the `edge` channel:

```shell
sudo snap install remmina  --edge
```

**Or**, update your Remmina Snap install with your channel of choice:

```shell
sudo snap refresh remmina --channel=edge # use --channel=stable otherwise
```

Snaps are updated with the system and include both latest `FreeRDP` Git and the latest `libssh 0.7` release (for better security).

## [Ubuntu](https://ubuntu.com/)

**The Remmina PPA is looking for a maintainer**, and it's not actively updated.

We may release new versions from time to time (as of writing is 1.4.23), Remmina can be installed by copying and pasting this in a terminal:

```
sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
sudo apt update
sudo apt install remmina remmina-plugin-rdp remmina-plugin-secret
```

Make sure Remmina is not running. Either close it, reboot, or kill it by pasting this in a terminal:

```
sudo killall remmina
```

List of available plugins with `apt-cache search remmina-plugin`. By default, RDP, SSH and SFTP are installed.
To test a daily built development version, do the above, but start with:

```
ppa:remmina-ppa-team/remmina-next-daily
```

## [Debian](https://www.debian.org/)

### 11 [Bullseye](https://wiki.debian.org/DebianBullseye) (stable), [Buster](https://wiki.debian.org/DebianBuster) (oldstable), and [Sid](https://wiki.debian.org/DebianUnstable) (unstable)

Remmina is in the official stable, testing and unstable repositories.

### 9 [Stretch](https://wiki.debian.org/DebianStretch) (oldstable)

Here Remmina can be [installed from backports](https://www.remmina.org/debian-the-boys-are-backport-in-town/) by copying and pasting this in a terminal:
```
echo 'deb http://ftp.debian.org/debian stretch-backports main' | sudo tee --append /etc/apt/sources.list.d/stretch-backports.list >> /dev/null
sudo apt update
sudo apt install -t stretch-backports remmina remmina-plugin-rdp remmina-plugin-secret remmina-plugin-spice
```

## [Raspberry Pi](https://www.raspberrypi.org/)

**ATTENTION**: We do not (or rarely) update this repository anymore. Maintainers are welcome if any.

Remmina operates its own package repository for Raspbian Buster (10.x) you can install by copying and pasting this in a terminal:

```
sudo apt install dirmngr
sudo apt-key adv --fetch-keys https://www.remmina.org/raspbian/remmina_raspbian.asc
sudo bash -c 'echo "deb https://www.remmina.org/raspbian/ buster main" > /etc/apt/sources.list.d/remmina_raspbian.list'
sudo apt update
sudo apt install remmina
```

(Tested on RPi 3B and 3B+)
It is strongly advised to also install gnome-keyring to let Remmina save encrypted passwords in a safer place than .remmina textfiles in the /home user folder.

```
sudo apt install gnome-keyring
```

(The remmina-next PPA can be used on Raspberry Pi 2 and 3, provided you are running one of the supported *buntu derivatives.)

## [Fedora](https://getfedora.org/) and [Red Hat](https://www.redhat.com)

As of March 2018 Remmina is available on most Fedora testing and stable, we still have a (not updated) Copr provided by [Hubbitus](https://github.com/Hubbitus) (Pavel Alexeev), to install, paste this in a terminal as root:

```shell
dnf copr enable hubbitus/remmina-next
dnf upgrade --refresh 'remmina*' 'freerdp*'
```

## [Red Hat/CentOS](https://www.redhat.com)

Install Remmina from the EPEL repository by pasting this in a terminal:

```shell
wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
```

A more updated version is available thanks to @castorsky through a [COPR repo](https://copr.fedorainfracloud.org/coprs/castor/remmina/)

To install Remmina on RHEL/CentOS 8 enable EPEL and COPR repo then install package with following commands:

```shell
dnf install epel-release
dnf copr enable castor/remmina
dnf install 'remmina*'
```

## [Gentoo](https://www.gentoo.org/)

To build and install

```shell
emerge -av net-misc/remmina
```

To build a binary and install

```shell
emerge -avb net-misc/remmina
```

```shell
emerge -avk remmina*.tar.bz
# From cp /usr/portage/packages/net-misc remmina* > /mnt/some-usb-stick to multiple boxes
emerge -avk =net-misc/remmina-#??Version number
```

## [Sabayon](https://www.sabayon.org/)

```
equo -i net-misc/remmina
 ```

## [Arch Linux](https://www.archlinux.org/) (and friends)

There is an official Arch Linux Remmina package. **Or**, if you want to use the AUR, install [remmina-git](https://aur.archlinux.org/packages/remmina-git).
There are also some external, not supported plugins provided by [Muflone](https://github.com/muflone) and other
Arch Remmina [Protocol plugins](https://aur.archlinux.org/packages/?O=0&SeB=nd&K=Remmina&outdated=&SB=n&SO=a&PP=50&do_Search=Go)

## [Solus](https://getsol.us/home/)

```shell
sudo eopkg update-repo
sudo eopkg install remmina
```

## [Build from source](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compilation)

- [Compilation guide for Red Hat/CentOS](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compilation-guide-for-RHEL)
- [Compile on Arch Linux](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Arch-Linux)
- [Compile on Debian 10 Buster](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Debian-10-Buster)
- [Compile on Debian 9 Stretch](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Debian-9-Stretch)
- [Compile on Fedora 20 ~ 26](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Fedora-20-~-26)
- [Compile on FreeBSD](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-FreeBSD)
- [Compile on Ubuntu 14.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-14.04)
- [Compile on Ubuntu 16.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-16.04)
- [Compile on Ubuntu 18.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-18.04)
- [Compile on Ubuntu 20.04](https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compile-on-Ubuntu-20.04)

Code development and bugfixes take place on the master branch.
It compiles with the current FreeRDP master branch, and will compile with [FreeDRP](http://www.freerdp.com/) 2.0 when released as stable.

# Links

- [Remmina website](https://www.remmina.org/)
- [Remmina subreddit](https://reddit.com/r/Remmina)
- [Remmina Development Doc](https://remmina.gitlab.io/remminadoc.gitlab.io)
- [Versions for various operating systems](https://repology.org/project/remmina/versions)
